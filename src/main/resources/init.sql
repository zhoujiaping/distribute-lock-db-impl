create table mutex_lock(
    id bigint primary key auto_increment,
    lock_key varchar(128) not null,
    lock_value varchar(128) not null,
    create_time datetime default now(),
    expire_at datetime,
    remark varchar(128)
);

alter table mutex_lock add unique index(lock_key);

alter table mutex_lock add index(lock_value);
alter table mutex_lock add index(expire_at);