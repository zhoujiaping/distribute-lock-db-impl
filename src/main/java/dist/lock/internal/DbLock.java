package dist.lock.internal;

import com.alibaba.druid.pool.DruidDataSource;
import fn.fn.Fn0;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.QueryRunner;

import java.sql.SQLException;
import java.util.Properties;

import static fn.Cores.*;

/**
 * 基于数据库实现分布式互斥锁
 * 不支持锁重入
 * 不支持锁自动续期
 * */
@Slf4j
public class DbLock implements Lock{
    static DruidDataSource druidDataSource = let(new DruidDataSource(),it->{
        Properties props = new Properties();
        props.setProperty("druid.url","jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf8");
        props.setProperty("druid.username","root");
        props.setProperty("druid.password","123456");
        props.setProperty("druid.driverClassName","com.mysql.cj.jdbc.Driver");
        props.setProperty("druid.initialSize","5");
        props.setProperty("druid.maxActive","20");
        props.setProperty("druid.minIdle","5");
        props.setProperty("druid.maxWait","60000");
        props.setProperty("druid.useUnfairLock","true");
        props.setProperty("druid.testWhileIdle","true");
        props.setProperty("druid.testOnBorrow","false");
        props.setProperty("druid.testOnReturn","false");
        props.setProperty("druid.validationQuery","SELECT 1 FROM DUAL");
        props.setProperty("druid.poolPreparedStatements","false");
        //props.setProperty("druid.filters","stat,wall,log4j");
        props.setProperty("druid.connectionProperties","druid.stat.mergeSql=true;druid.stat.slowSqlMillis=500");
        props.setProperty("druid.useGlobalDataSourceStat","true");
        props.setProperty("druid.maxPoolPreparedStatementPerConnectionSize","20");
        props.setProperty("druid.minEvictableIdleTimeMillis","300000");
        props.setProperty("druid.timeBetweenEvictionRunsMillis","60000");
        it.configFromPropety(props);
    });
    static QueryRunner queryRunner = new QueryRunner(druidDataSource);
    static volatile boolean expireLockClearThreadStarted;

    public static synchronized void startClearExpiredLock(){
        if(!expireLockClearThreadStarted){
            expireLockClearThreadStarted = true;
            setInterval(0,1000,()->{
                //删除过期的锁
                clearExpireLock0();
            });
        }
    }
    @SneakyThrows
    private static int clearExpireLock0(){
        return queryRunner.execute("delete from mutex_lock where expire_at < now();");
    }
    @Override
    @SneakyThrows
    public boolean lock(String key, String value, long expireMillis) {
        try {
            int count = queryRunner.execute("insert into mutex_lock(lock_key,lock_value,create_time,expire_at)" +
                    "values(?,?,now(),date_add(now(), interval ? microsecond));", key, value, expireMillis * 1000);
            return count == 1;
        }catch (SQLException e){
            log.info(e.getMessage());
            return false;
        }
    }

    @Override
    @SneakyThrows
    public boolean unlock(String key, String value) {
        int count = queryRunner.execute("delete from mutex_lock where lock_key = ? and lock_value = ?;",key,value);
        return count == 1;
    }

    public static <T> T withLock(String key, String value, long expireMillis, Fn0<T> fn){
        DbLock dbLock = new DbLock();
        if(dbLock.lock(key,value,expireMillis)){
            try{
                return fn.invoke();
            }finally {
                dbLock.unlock(key,value);
            }
        }else{
            throw new RuntimeException("cannot get mutex lock!");
        }
    }
}
