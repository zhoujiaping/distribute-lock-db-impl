package dist.lock.internal;

public interface Lock {
    boolean lock(String key,String value,long timeoutMillis);
    boolean unlock(String key,String value);
}
