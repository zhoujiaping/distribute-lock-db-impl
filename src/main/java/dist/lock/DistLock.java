package dist.lock;

import fn.fn.Fn0;

public abstract class DistLock {
    public static <T> T withLock(Fn0<T> fn){
        return fn.invoke();
    }
}
