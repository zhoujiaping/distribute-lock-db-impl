import dist.lock.internal.DbLock;
import org.junit.jupiter.api.Test;

import static fn.Cores.*;

public class DbLockTest {
    @Test
    public void testLock() {
        DbLock.startClearExpiredLock();
        DbLock dbLock = new DbLock();
        String key = "loan#000001";
        String value = "1234";
        if (dbLock.lock(key, value, 1000)) {
            println("do in lock...");
            dbLock.unlock(key,value);
        }
        println("end...");
        sleep(10000);
    }
    @Test
    public void testWithLock() {
        DbLock.startClearExpiredLock();
        String key = "loan#000001";
        String value = "1234";
        DbLock.withLock(key,value,10000,()-> println("do in lock..."));
        println("end...");
        sleep(10000);
    }
}
